import logo from './logo.svg';
import './App.css';
import Header from './Header/Header';
import PageContent from './PageContent/PageContent';

function App() {
  return (
    <div className="App">
      <Header />
      <PageContent/>
    </div>
  );
}

export default App;
