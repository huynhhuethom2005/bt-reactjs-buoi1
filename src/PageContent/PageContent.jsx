import React, { Component } from 'react'
import Card from '../Card/Card'

export default class PageContent extends Component {
    render() {
        return (
            <div>
                <section class="pt-4">
                    <div class="container px-lg-5">
                        <div class="row gx-lg-5">
                            <Card/>
                            <Card/>
                            <Card/>
                            <Card/>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}
